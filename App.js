import React from 'react';
import { reduxifyNavigator } from 'react-navigation-redux-helpers';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider, connect } from 'react-redux';
import { Root } from 'native-base';

import { appContainer } from './src/config/navigators';
import { store, persistor } from './src/store/configureStore';

const app = reduxifyNavigator(appContainer, 'root');

const mapStateToProps = state => {
      return {
            state: state.navReducer
      }
}

const AppWithNavigationState = connect(mapStateToProps)(app);

export default class App extends React.Component {
      render() {
            return (
                  <Provider store={store}>
                        <PersistGate loading={null} persistor={persistor}>
                              <Root>
                                    <AppWithNavigationState />
                              </Root>
                        </PersistGate>
                  </Provider>
            )
      }
}