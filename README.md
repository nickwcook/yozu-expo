## Yozu Updates by Nick Cook

### Developed with React Native via Expo CLI

#### Implementing:

* react-navigation
* react-redux
* react-navigation-redux-helpers
* redux-persist
* redux-persist-transform-filters
* expo-jwt
* moment
* native-base

____

#### Starting the Application

**Download** the application by clicking on '_Clone_' to the top-right of the repository home page, copying the highlighted command in the following modal and then pasting the text into a terminal/command prompt opened via a folder from which you wish to access the application.

**Open** the application directory by running the command '_cd yozu-expo_' in the terminal.

**Install** the application's dependencies by running the command '_yarn install_'.

**Start** the application script by running the command '_yarn start_'.

**Open** the application via the Expo app by scanning the QR code shown in the terminal using the iOS camera app or the Expo app for Android.

____

#### Technical Notes:

File '_auth\_credentials.json_' not added to .gitignore for purposes of this project and ease of application startup.

Architected using Container/Component pattern.

Component code written to match standards described in AirBnB React style guide.

Redux persistence configured using redux-persist package 'storage' library with navigation state blacklisted (however not required for this particular scenario given SplashScreen component will perform checks on authorisation on application bootstrapping prior to selectively redirecting to either AuthStack or AppStack stack navigators). Package 'redux-persist-transform-filter' used for blacklisting selected reducer properties, notably property 'error' on authReducer.

###### Filtering Updates:

I had investigated an approach using Reselect for Redux, however opted for the use of manual filtering via  _applyFilters()_ at the UpdatesListScreen component, receiving selected usernames and selected tags from the FilterUpdatesDialog component.

I decided to use this approach as Reselect may be better for performing filtering directly within the Redux action creators, but does not seem an optimal solution for this use case, in which the array is filtered on a potentially large number of conditions, matching to various selected usernames and tags, which can instead be iterated-over easily at view-level.

###### Styling:

Having investigated component styling patterns for React Native, for which there seems to be no common approach, I have decided upon an approach whereby generic element styles and utilities can be accessed via /styles/base.js and unique styling is performed via StyleSheets within component files. I have attempted to limit the use of inline styles to elements that only have 1-2 unique style properties, and have used StyleSheet declarations where components have various styled elements or elements have larger blocks of styling properties.