import JWT from 'expo-jwt';

import { AUTH_SIGNIN_SUCCESS, AUTH_SIGNIN_FAILURE, AUTH_SIGNOUT } from '../constants/actionTypes';
import users from '../users.json';
import auth_credentials from '../auth_credentials.json';

export function signIn(requestUsername, requestPassword) {
      const matchingUser = users.find(user => user.username === requestUsername);

      if (!matchingUser) {
            return {
                  type: AUTH_SIGNIN_FAILURE,
                  error: `No user found with username '${requestUsername}'`
            }
      } else if (requestPassword !== matchingUser.password) {
            return {
                  type: AUTH_SIGNIN_FAILURE,
                  error: 'Incorrect password'
            }
      } else {
            return {
                  type: AUTH_SIGNIN_SUCCESS,
                  username: matchingUser.username,
                  token: generateToken(matchingUser.username)
            }
      }
}

export function signOut() {
      return {
            type: AUTH_SIGNOUT
      }
}

function generateToken(username) {
      let expiry = new Date();
      expiry.setDate(expiry.getTime() + 7);

      return JWT.encode({
            iss: username,
            exp: expiry
      }, auth_credentials.token_key)
}