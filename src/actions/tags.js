import { TAGS_ADD_TAG } from '../constants/actionTypes';

export function addTag({ text, username }) {
      return {
            type: TAGS_ADD_TAG,
            text,
            username
      }
}