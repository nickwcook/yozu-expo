import { UPDATES_ADD_UPDATE, UPDATES_FILTER_UPDATES } from '../constants/actionTypes';

export function addUpdate({ text, tags, username }) {
      return {
            type: UPDATES_ADD_UPDATE,
            text,
            tags,
            username
      }
}

export function filterUpdates(selectedUsernames, selectedTags) {
      return {
            type: UPDATES_FILTER_UPDATES,
            selectedUsernames,
            selectedTags
      }
}