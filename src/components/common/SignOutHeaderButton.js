import React from 'react';
import { Button } from 'react-native';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { Toast } from 'native-base';

import { signOut } from '../../actions/auth';

class SignOutHeaderButton extends React.Component {

      constructor(props) {
            super(props);
      }

      signOut = () => {
            this.props.signOut();

            Toast.show({
                  text: 'Signed out',
                  duration: 3000
            })
            
            this.props.navigation.navigate('AuthStack');
      }

      render() {
            return (
                  <Button
                        title="Sign Out"
                        onPress={this.signOut}
                  />
            )
      }
}

const mapDispatchToProps = {
      signOut
}

export default connect(null, mapDispatchToProps)(withNavigation(SignOutHeaderButton))