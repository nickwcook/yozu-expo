import React from 'react';
import { Form, Item, Input } from 'native-base';

import { forms } from '../../styles/base';

const LoginForm = ({ handleInputChange }) => {
      return (
            <Form>
                <Item style={forms.formItem}>
                    <Input
                        onChangeText={value => handleInputChange(value, 'username')}
                        placeholder="Username"
                  />
                </Item>
                <Item style={forms.formItem}>
                    <Input
                        onChangeText={value => handleInputChange(value, 'password')}
                        placeholder="Password"
                        secureTextEntry={true}
                  />
                </Item>
            </Form>
      )
}

export default LoginForm;