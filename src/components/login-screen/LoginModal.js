import React from 'react';
import { View, Card, Text, Button, Form, Item, Input, H3 } from 'native-base';

import { margins, text, button, modals } from '../../styles/base';
import LoginForm from './LoginForm';

export default class LoginModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: null,
            password: null
        }
    }

    handleInputChange = (newValue, inputName) => {
          this.setState({
                [inputName]: newValue
          })
    }

    signIn = () => {
        this.props.signIn(this.state.username, this.state.password);
    }

    render() {
        return (
            <Card style={modals.modal}>
                <H3 style={modals.modalTitle}>Sign In</H3>
                <LoginForm handleInputChange={this.handleInputChange} />
                <View style={modals.modalActions}>
                        <Button
                              onPress={this.signIn}
                              disabled={!this.state.username || !this.state.password}
                              style={{...button}}
                        >
                              <Text>Sign In</Text>
                        </Button>
                </View>
            </Card>
        )
    }
}