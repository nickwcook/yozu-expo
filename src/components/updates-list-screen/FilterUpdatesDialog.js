import React from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { View, Container, Card, Button, Text, H3, Accordion, Separator } from 'native-base';

import users from '../../users.json';
import { modals } from '../../styles/base';
import TagsGrid from './TagsGrid';
import UsersChecklist from './UsersChecklist';

export default class FilterUpdatesDialog extends React.Component {

      constructor(props) {
            super(props);

            this.state = {
                  selectedUsernames: [],
                  selectedTags: []
            }
      }

      componentDidMount() {
            this.setState({
                  selectedUsernames: this.props.activeFilters.selectedUsernames,
                  selectedTags: this.props.activeFilters.selectedTags
            })
      }

      getAllUsernames() {
            return users.map(user => user.username);
      }

      toggleTagSelected = (toggledTag) => {
            if (this.isTagSelected(toggledTag)) {
                  this.setState({
                        selectedTags: this.state.selectedTags.filter(selectedTag => (selectedTag.id != toggledTag.id))
                  })
            } else {
                  this.setState({
                        selectedTags: [
                              ...this.state.selectedTags,
                              toggledTag
                        ]
                  })
            }
      }

      isTagSelected = (toggledTag) => {
            let isSelected = false;

            for (let selectedTag of this.state.selectedTags) {
                  if (selectedTag.id == toggledTag.id) {
                        isSelected = true;
                        break;
                  }
            }

            return isSelected;
      }

      toggleUserSelected = (toggledUsername) => {
            if (this.isUsernameSelected(toggledUsername)) {
                  this.setState({
                        selectedUsernames: this.state.selectedUsernames.filter(selectedUsername => selectedUsername !== toggledUsername)
                  })
            } else {
                  this.setState({
                        selectedUsernames: [
                              ...this.state.selectedUsernames,
                              toggledUsername
                        ]
                  })
            }
      }

      isUsernameSelected = (toggledUsername) => {
            let isSelected = false;

            for (let selectedUsername of this.state.selectedUsernames) {
                  if (selectedUsername === toggledUsername) {
                        isSelected = true;
                        break;
                  }
            }

            return isSelected;
      }

      applyFilters = () => {
            this.props.applyFilters(this.state.selectedUsernames, this.state.selectedTags);
      }

      render() {
            return (
                  <Container style={modals.modalContainer}>
                        <Card style={modals.modal}>
                              <H3 style={modals.modalTitle}>Filter Updates</H3>
                              <ScrollView>
                                    <UsersChecklist
                                          usernames={this.getAllUsernames()}
                                          selectedUsernames={this.state.selectedUsernames}
                                          toggleUserSelected={this.toggleUserSelected}
                                    />
                                    <View style={{ marginTop: 20 }} />
                                    <TagsGrid
                                          tags={this.props.tags}
                                          selectedTags={this.state.selectedTags}
                                          toggleTagSelected={this.toggleTagSelected}
                                    />
                              </ScrollView>
                              <View style={modals.modalActions}>
                                    <Button
                                          onPress={this.props.closeDialog}
                                          style={modals.modalActionsButton}
                                    >
                                          <Text>Cancel</Text>
                                    </Button>
                                    <Button
                                          onPress={this.applyFilters}
                                          style={modals.modalActionsButton}
                                    >
                                          <Text>Apply</Text>
                                    </Button>
                              </View>
                        </Card>
                  </Container>
            )
      }
}