import React from 'react';
import { ScrollView } from 'react-native';
import { Container, View, Card, Text, Button, Form, Item, Textarea, H3, Toast } from 'native-base';
import { connect } from 'react-redux';

import { modals } from '../../styles/base';
import TagsGrid from './TagsGrid';
import NewUpdateForm from './NewUpdateForm';
import { addUpdate } from '../../actions/updates';

class NewUpdateDialog extends React.Component {

      constructor(props) {
            super(props);

            this.state = {
                  text: null,
                  selectedTags: []
            }
      }

      handleTextChange = (newValue) => {
            this.setState({
                  text: newValue
            })
      }

      toggleTagSelected = (toggledTag) => {
            if (this.isTagSelected(toggledTag)) {
                  this.setState({
                        selectedTags: this.state.selectedTags.filter(selectedTag => (selectedTag.id != toggledTag.id))
                  })
            } else {
                  this.setState({
                        selectedTags: [
                              ...this.state.selectedTags,
                              toggledTag
                        ]
                  })
            }
      }

      isTagSelected = (toggledTag) => {
            let isSelected = false;

            for (let selectedTag of this.state.selectedTags) {
                  if (selectedTag.id == toggledTag.id) {
                        isSelected = true;
                        break;
                  }
            }

            return isSelected;
      }

      saveUpdate = () => {
            this.props.addUpdate({
                  text: this.state.text,
                  tags: this.state.selectedTags,
                  username: this.props.auth.username
            })

            Toast.show({
                  text: 'Update saved',
                  buttonText: 'Close',
                  type: 'success',
                  duration: 3000
            })

            this.props.closeDialog();
      }

      render() {
            return (
                  <Container style={modals.modalContainer}>
                        <Card style={modals.modal}>
                              <H3 style={modals.modalTitle}>New Update</H3>
                              <ScrollView>
                                    <NewUpdateForm handleTextChange={this.handleTextChange} />
                                    <TagsGrid
                                          tags={this.props.tags}
                                          selectedTags={this.state.selectedTags}
                                          toggleTagSelected={this.toggleTagSelected}
                                    />
                              </ScrollView>
                              <View style={modals.modalActions}>
                                    <Button
                                          onPress={this.props.closeDialog}
                                          style={modals.modalActionsButton}
                                    >
                                          <Text>Cancel</Text>
                                    </Button>
                                    <Button
                                          onPress={this.saveUpdate}
                                          disabled={!this.state.text || !this.state.selectedTags.length}
                                          style={modals.modalActionsButton}
                                    >
                                          <Text>Save</Text>
                                    </Button>
                              </View>
                        </Card>
                  </Container>
            )
      }
}

const mapStateToProps = (state) => {
      return {
            auth: state.authReducer,
            tags: state.tagsReducer
      }
}

const mapDispatchToProps = {
      addUpdate
}

export default connect(mapStateToProps, mapDispatchToProps)(NewUpdateDialog);