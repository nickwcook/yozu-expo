import React from 'react';
import { ScrollView } from 'react-native';
import { Form, Item, Textarea } from 'native-base';

import { forms } from '../../styles/base';

const NewUpdateForm = ({ handleTextChange }) => {
      return (
            <Form>
                  <Item style={forms.formItem}>
                        <Textarea
                              onChangeText={value => handleTextChange(value)}
                              placeholder="Today I have..."
                              rowSpan={4}
                              bordered
                              style={forms.formInput}
                        />
                  </Item>
            </Form>
      )
}

export default NewUpdateForm;