import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Container, Button, Text } from 'native-base';

import theme from '../../styles/theme';

const TagsGrid = ({ tags, selectedTags, toggleTagSelected }) => {

      function isTagSelected(tag) {
            let isSelected = false;

            for (let selectedTag of selectedTags) {
                  if (selectedTag.id == tag.id) {
                        isSelected = true;
                        break;
                  }
            }

            return isSelected;
      }

      return (
            <View>
                  <Text>Tags:</Text>
                  <View style={styles.tagsGrid}>
                        {tags.map((tag, index) => (
                              <Button
                                    onPress={() => toggleTagSelected(tag)}
                                    key={tag.id}
                                    style={[styles.tag, (isTagSelected(tag) ? styles.tagSelected : null)]}
                                    small
                                    rounded
                              >
                                    <Text>{tag.text}</Text>
                              </Button>
                        ))}
                  </View>
            </View>
      )
}

const styles = StyleSheet.create({
    tagsGrid: {
        marginTop: 10,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    tag: {
        marginRight: 10,
        marginBottom: 10,
        backgroundColor: theme.primary
    },
    tagSelected: {
        backgroundColor: theme.primaryDark
    }
})

export default TagsGrid;