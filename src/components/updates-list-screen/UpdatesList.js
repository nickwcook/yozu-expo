import React from 'react';
import { ScrollView } from 'react-native';

import UpdatesListItem from './UpdatesListItem';

const UpdatesList = ({ updates }) => {
    return (
        <ScrollView style={{ paddingLeft: 20, paddingRight: 20 }}>
            {updates.map((update, index) => (
                  <UpdatesListItem
                        update={update}
                        key={update.id}
                        isLastChild={(update.id == updates.length)}
                  />
            ))}
        </ScrollView>
    )
}

export default UpdatesList;