import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Button, Text, Icon } from 'native-base';

import { flex, padding, margins, button } from '../../styles/base';

const UpdatesListHeader = ({ updates, totalUpdatesLength, tags, showFiltersDialog }) => {

      function renderUpdatesLength() {
            if (totalUpdatesLength == 0) {
                  return <Text>No updates</Text>
            } else if (totalUpdatesLength == 1) {
                  return <Text>Showing {updates.length} of 1 update</Text>
            } else {
                  return <Text>Showing {updates.length} of {totalUpdatesLength} updates</Text>
            }
      }

      return (
            <View style={styles.container}>
                  {renderUpdatesLength()}
                  <Button
                        onPress={showFiltersDialog}
                        style={{...button}}
                  >
                        <Text>Filter</Text>
                        <Icon name="switch" />
                  </Button>
            </View>
      )
}

const styles = StyleSheet.create({
      container: {
            padding: 20,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
      }
})

export default UpdatesListHeader;