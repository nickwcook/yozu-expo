import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Card, CardItem, Body, Left, Right, Text, Icon } from 'native-base';
import moment from 'moment';

import theme from '../../styles/theme';

const UpdatesListItem = ({ update, isLastChild }) => {
      const formattedDate = moment(update.date).format('DD/MM/YYYY HH:mm');

      return (
            <Card style={[styles.card, isLastChild ? styles.lastCard : null]}>
                  <CardItem header>
                        <Left>
                              <Icon
                                    name="contact" 
                                    style={styles.textLight}
                              />
                              <Text>{update.username} posted</Text>
                        </Left>
                  </CardItem>
                  <CardItem>
                        <Body>
                              <Text>{update.text}</Text>
                              <Text style={styles.tagsHeader}>Tags:</Text>
                              <View style={styles.tagsContainer}>
                                    {update.tags.map((tag, index) => (
                                          <View style={styles.tagChip} key={tag.id}>
                                                <Text style={styles.tagLabel}>{tag.text}</Text>
                                          </View>
                                    ))}
                              </View>
                        </Body>
                  </CardItem>
                  <CardItem footer style={styles.cardFooter}>
                        <Right>
                              <Text style={styles.textLight}>{formattedDate}</Text>
                        </Right>
                  </CardItem>
            </Card>
      )
}

const styles = StyleSheet.create({
    card: {
      width: '100%',
      marginBottom: 20
    },
    tagsHeader: {
      marginTop: 20,
      fontWeight: '600'
    },
    tagsContainer: {
      marginTop: 10,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap'
    },
    tagChip: {
      marginRight: 8,
      padding: 8,
      backgroundColor: theme.primaryDark,
      borderRadius: 5
    },
    tagLabel: {
      fontSize: 14,
      color: '#fff'
    },
    cardFooter: {
      display: 'flex',
      justifyContent: 'flex-end'
    },
    lastCard: {
      marginBottom: 0
    },
    textLight: {
      color: theme.textTertiary
    }
})

export default UpdatesListItem;