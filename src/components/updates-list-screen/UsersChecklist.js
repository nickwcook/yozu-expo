import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { View, Text, ListItem, CheckBox, Body } from 'native-base';

const UsersChecklist = ({ usernames, selectedUsernames, toggleUserSelected }) => {      
      return (
            <View>
                  <Text>Users:</Text>
                  <View style={{ marginTop: 10 }}>
                        {usernames.map((username, index) => (
                              <TouchableOpacity
                                    onPress={() => toggleUserSelected(username)}
                                    key={index}
                                    style={styles.listItem}
                              >
                                    <CheckBox
                                          onPress={() => toggleUserSelected(username)}
                                          checked={selectedUsernames.includes(username)}
                                          style={styles.checkbox}
                                    />
                                    <Text>{username}</Text>
                              </TouchableOpacity>
                        ))}
                  </View>
            </View>
      )
}

const styles = StyleSheet.create({
      listItem: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginBottom: 15
      },
      checkbox: {
            marginLeft: -10,
            marginRight: 20
      }
})

export default UsersChecklist;