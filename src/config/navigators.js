import React from 'react';
import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/LoginScreen';
import UpdatesListScreen from '../screens/UpdatesListScreen';
import SignOutHeaderButton from '../components/common/SignOutHeaderButton';

export const authStackNavigator = createStackNavigator(
      { 
            Login: {
                  screen: LoginScreen
            }
      },
      {
            defaultNavigationOptions: {
                  header: null
            }
      }
)

export const appStackNavigator = createStackNavigator(
      {
            UpdatesList: {
                  screen: UpdatesListScreen
            }
      },
      {
            defaultNavigationOptions: {
                  headerRight: <SignOutHeaderButton />
            }
      }
)

export const appContainer = createAppContainer(createSwitchNavigator(
      {
            SplashScreen,
            AuthStack: authStackNavigator,
            AppStack: appStackNavigator
      },
      {
            initialRouteName: 'SplashScreen'
      }
))