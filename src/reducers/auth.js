import { AUTH_SIGNIN_SUCCESS, AUTH_SIGNIN_FAILURE, AUTH_SIGNOUT } from '../constants/actionTypes';

const initialState = {
      error: null,
      username: null,
      token: null
}

export default function authReducer(state = initialState, action) {
      switch(action.type) {
            case AUTH_SIGNIN_SUCCESS:
                  return {
                        error: null,
                        username: action.username,
                        token: action.token
                  }
            case AUTH_SIGNIN_FAILURE:
                  return {
                        error: action.error,
                        username: null,
                        token: null
                  }
            case AUTH_SIGNOUT:
                  return {
                        error: null,
                        username: null,
                        token: null
                  }
            default:
                  return state;
      }
}