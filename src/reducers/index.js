import { combineReducers } from 'redux';

import authReducer from './auth';
import navReducer from './nav';
import tagsReducer from './tags';
import updatesReducer from './updates';

const rootReducer = combineReducers({
      authReducer,
      navReducer,
      tagsReducer,
      updatesReducer
})

export default rootReducer;