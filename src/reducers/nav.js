import { createNavigationReducer } from 'react-navigation-redux-helpers';

import { appContainer } from '../config/navigators';

const navReducer = createNavigationReducer(appContainer);

export default navReducer;