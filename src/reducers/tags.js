import { TAGS_ADD_TAG } from '../constants/actionTypes';

const initialState = [
      {
            id: 1,
            text: 'ReactJS',
            createdBy: 'nick.cook'
      },
      {
            id: 2,
            text: 'React Native',
            createdBy: 'user.two'
      },
      {
            id: 3,
            text: 'Redux',
            createdBy: 'nick.cook'
      }
]

export default function tagsReducer(state = initialState, action) {
      switch(action.type) {
            case TAGS_ADD_TAG:
                  return [
                        ...state,
                        {
                              text: action.text,
                              createdBy: action.username
                        }
                  ]
            default:
                  return [
                        {
                              id: 1,
                              text: 'ReactJS',
                              createdBy: 'nick.cook'
                        },
                        {
                              id: 2,
                              text: 'React Native',
                              createdBy: 'user.two'
                        },
                        {
                              id: 3,
                              text: 'Redux',
                              createdBy: 'nick.cook'
                        }
                  ]
      }
}