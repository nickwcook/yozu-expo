import { UPDATES_ADD_UPDATE } from '../constants/actionTypes';

const initialState = [];

export default function updatesReducer(state = initialState, action) {
      switch(action.type) {
            case UPDATES_ADD_UPDATE:
                  return [
                        ...state,
                        {
                              id: state.length,
                              text: action.text,
                              tags: action.tags,
                              username: action.username,
                              date: new Date().toISOString()
                        }
                  ]
            default:
                  return state;
      }
}