import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, H3, Toast } from 'native-base';
import { connect } from 'react-redux';

import theme from '../styles/theme';
import { signIn } from '../actions/auth';
import LoginModal from '../components/login-screen/LoginModal';

class LoginScreen extends React.Component {

      constructor(props) {
            super(props);
      }

      static navigationOptions = ({ navigation }) => {
            return {
                  headerTitle: 'Yozu Updates'
            }
      }

      signIn = (username, password) => {
            this.props.signIn(username, password);
      }

      componentWillReceiveProps(nextProps) {
            this.checkSignInStatus(nextProps.auth);
      }

      checkSignInStatus(authStatus) {
            if (authStatus.token) {
                  Toast.show({
                        text: `Signed in as ${authStatus.username}`,
                        buttonText: 'Close',
                        duration: 3000
                  })
                  this.props.navigation.navigate('AppStack');
            } else {
                  Toast.show({
                        text: `Error: ${authStatus.error}`,
                        buttonText: 'Close',
                        type: 'danger',
                        duration: 5000
                  })
            }
      }

      render() {
            return (
                  <Container style={styles.container}>
                        <H3 style={styles.title}>Yozu Updates</H3>
                        <LoginModal
                              signIn={this.signIn}
                        />
                  </Container>
            )
      }
}

const styles = StyleSheet.create({
      container: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: theme.primary
      },
      title: {
            fontSize: 24,
            fontWeight: '600',
            color: '#fff',
            marginBottom: 20
      }
})

const mapStateToProps = state => {
      return {
            auth: state.authReducer
      }
}

const mapDispatchToProps = {
      signIn
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);