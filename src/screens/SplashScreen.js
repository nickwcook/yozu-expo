import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, Spinner } from 'native-base';
import { connect } from 'react-redux';
import JWT from 'expo-jwt';
import moment from 'moment';

import auth_credentials from '../auth_credentials.json';

class SplashScreen extends React.Component {

      constructor(props) {
            super(props);
      }

      componentDidMount() {
            this.checkAuthorisation();
      }
      
      checkAuthorisation() {
            if (!this.props.auth.token) {
                  this.props.navigation.navigate('AuthStack');
            } else {
                  const { exp: expiry } = JWT.decode(this.props.auth.token, auth_credentials.token_key);

                  if (moment(expiry).isBefore(moment())) {
                        this.props.navigation.navigate('AuthStack');
                  } else {
                        this.props.navigation.navigate('AppStack');
                  }
            }
      }

      render() {
            return (
                  <Container style={styles.container}>
                        <Spinner />
                  </Container>
            )
      }
}

const styles = StyleSheet.create({
      container: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
      }
})

const mapStateToProps = state => {
      return {
            auth: state.authReducer
      }
}

export default connect(mapStateToProps)(SplashScreen);