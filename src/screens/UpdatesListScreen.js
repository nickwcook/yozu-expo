import React from 'react';
import { ScrollView } from 'react-native';
import { View, Container, Content, Text, Fab, Icon } from 'native-base';
import { connect } from 'react-redux';

import { screens } from '../styles/base';
import theme from '../styles/theme';
import UpdatesListHeader from '../components/updates-list-screen/UpdatesListHeader';
import UpdatesList from '../components/updates-list-screen/UpdatesList';
import NewUpdateDialog from '../components/updates-list-screen/NewUpdateDialog';
import FilterUpdatesDialog from '../components/updates-list-screen/FilterUpdatesDialog';

class UpdatesListScreen extends React.Component {

      constructor(props) {
            super(props);

            this.state = {
                  updates: [],
                  showNewUpdateDialog: false,
                  showFiltersDialog: false,
                  activeFilters: {
                        selectedUsernames: [],
                        selectedTags: []
                  }
            }
      }
      
      static navigationOptions = ({ navigation }) => {
            return {
                  headerTitle: 'Updates'
            }
      }

      componentDidMount() {
            this.setState({
                  updates: this.props.updates
            })
      }

      componentWillReceiveProps(nextProps) {
            this.setState({
                  updates: nextProps.updates
            })
      }

      toggleNewUpdateDialog = () => {
            this.setState({
                  showNewUpdateDialog: !this.state.showNewUpdateDialog
            })
      }

      toggleFiltersDialog = () => {
            this.setState({
                  showFiltersDialog: !this.state.showFiltersDialog
            })
      }

      renderNewUpdateDialog() {
            if (this.state.showNewUpdateDialog) {
                  return <NewUpdateDialog closeDialog={this.toggleNewUpdateDialog} />
            } else {
                  return (
                        <Fab
                              onPress={this.toggleNewUpdateDialog}
                              style={{ backgroundColor: theme.primaryDark }}
                        >
                              <Icon name="md-add" />
                        </Fab>
                  )
            }
      }

      renderFiltersDialog() {
            if (this.state.showFiltersDialog) {
                  return (
                        <FilterUpdatesDialog
                              closeDialog={this.toggleFiltersDialog}
                              tags={this.props.tags}
                              applyFilters={this.applyFilters}
                              activeFilters={this.state.activeFilters}
                        />
                  )
            } else {
                  return null;
            }
      }

      applyFilters = (selectedUsernames, selectedTags) => {
            const filtered = this.props.updates.filter(update => {
                  if (!selectedUsernames.length && !selectedTags.length) {
                        return true;
                  } else if (!selectedUsernames.length && this.updateIncludesSelectedTag(update, selectedTags)) {
                        return true;
                  } else if (!selectedTags.length && this.updateMatchesUsername(update, selectedUsernames)) {
                        return true;
                  } else if (this.updateMatchesUsername(update, selectedUsernames) && this.updateIncludesSelectedTag(update, selectedTags)) {
                        return true;
                  } else {
                        return false;
                  }
            })

            this.setState({
                  updates: filtered,
                  activeFilters: {
                        selectedUsernames,
                        selectedTags
                  }
            })

            this.toggleFiltersDialog();
      }

      updateMatchesUsername(update, selectedUsernames) {
            let matchesUsername = false;

            for (let selectedUsername of selectedUsernames) {
                  if (selectedUsername === update.username) {
                        matchesUsername = true;
                        break;
                  }
            }

            return matchesUsername;
      }

      updateIncludesSelectedTag(update, selectedTags) {
            let includesTag = false;
            const updateTagIds = update.tags.map(tag => tag.id);
            const selectedTagIds = selectedTags.map(tag => tag.id);
            
            for (let tagId of selectedTagIds) {
                  if (updateTagIds.includes(tagId)) {
                        includesTag = true;
                        break;
                  }
            }

            return includesTag;
      }

      render() {
            return (
                  <Container style={screens.screenContainer}>
                        <Content>
                              <UpdatesListHeader
                                    updates={this.state.updates}
                                    totalUpdatesLength={this.props.updates.length}
                                    tags={this.props.tags}
                                    showFiltersDialog={this.toggleFiltersDialog}
                              />
                              <UpdatesList updates={this.state.updates} />
                        </Content>
                        {this.renderNewUpdateDialog()}
                        {this.renderFiltersDialog()}
                  </Container>
            )
      }
}

const mapStateToProps = state => {
      return {
            updates: state.updatesReducer,
            tags: state.tagsReducer
      }
}

export default connect(mapStateToProps)(UpdatesListScreen);