import { createStore, applyMiddleware, compose } from 'redux';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import storage from 'redux-persist/lib/storage';
import { persistStore, persistReducer } from 'redux-persist';
import { createBlacklistFilter } from 'redux-persist-transform-filter';

import rootReducer from '../reducers/index';
import { create } from 'uuid-js';

const authFilter = createBlacklistFilter(
      'authReducer',
      [
            'error'
      ]
)

const persistConfig = {
      key: 'root',
      storage,
      blacklist: [
            'navigation'
      ],
      transforms: [
            authFilter
      ]
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const navigationReduxMiddleware = createReactNavigationReduxMiddleware('root', state => state.nav);

export const store = createStore(persistedReducer, applyMiddleware(navigationReduxMiddleware));
export const persistor = persistStore(store);