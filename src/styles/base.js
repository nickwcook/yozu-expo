import { StyleSheet, Dimensions } from 'react-native';
import theme from './theme';

export const dimensions = {
    fullHeight: Dimensions.get('window').height,
    fullWidth: Dimensions.get('window').width
}

export const flex = StyleSheet.create({
    alignItemsStart: {
        display: 'flex',
        alignItems: 'flex-start'
    },
    alignItemsCenter: {
        display: 'flex',
        alignItems: 'center'
    },
    justifyContentCenter: {
        display: 'flex',
        justifyContent: 'center'
    },
    justifyContentBetween: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    row: {
          display: 'flex',
          flexDirection: 'row'
    }
})

export const screens = StyleSheet.create({
      screenContainer: {
            backgroundColor: '#f0f0f0'
      }
})

export const button = {
      backgroundColor: theme.primaryDark
}

export const modals = StyleSheet.create({
    modalContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        ...flex.alignItemsCenter,
        ...flex.justifyContentCenter
    },
    modal: {
        width: '90%',
        padding: 20,
        borderRadius: 5
    },
    modalTitle: {
        fontSize: 19,
        fontWeight: '600',
        marginBottom: 20
    },
    modalActions: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginTop: 20
    },
    modalActionsButton: {
        marginLeft: 10,
        ...button
    }
})

export const forms = StyleSheet.create({
    formItem: {
        marginLeft: 0,
        marginBottom: 20
    },
    formInput: {
        width: '100%'
    },
    formError: {
        color: 'red',
        fontSize: 14
    }
})