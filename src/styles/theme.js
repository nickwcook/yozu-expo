const Theme = {
    primaryDark: '#1976D2',
    primary: '#2196F3',
    primaryLight: '#BBDEFB',
    accent: '#FF5722',
    textPrimary: '#212121',
    textSecondary: '#727272',
    textTertiary: '#B6B6B6'
}

export default Theme;